﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractice
{
   public partial class PartialClassOne  : ICustomer
    // : Customer
    // does not support multiple inheritance
    // but support to implement multiple interfaces
    {
        public void CustomerMethod()
        {
            throw new NotImplementedException();
        }

        public void GetFullName()
        {
            Console.WriteLine(@"Full Name is : {0}{1}", _empFName, _empLName);
        }

       
    }

    public class Customer
    {
       
    }

    public interface ICustomer
    {
        void CustomerMethod();
    }
}
