﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            PartialClassOne obj = new PartialClassOne();
            obj.GetFullName();
            
            employee oemployee = new employee();
            oemployee.PartialMethod();

            Console.ReadLine();
        }
    }
}
