﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractice
{
   public partial class PartialClassOne  : IEmployee
        //: employee  
        // does not support multiple inheritance
        // but support to implement multiple interfaces
    {
        private int _empID;
        private string _empFName;
        private decimal _empSalary;
        private string _empLName;

        public int empID { get => _empID; set => _empID = value; }
        public string empFName { get => _empFName; set => _empFName = value; }
        public decimal empSalary { get => _empSalary; set => _empSalary = value; }
        public string empLName { get => _empLName; set => _empLName = value; }

        public void EmployeeMethod()
        {
            throw new NotImplementedException();
        }

        //declare one partial method
       partial void PartialMethod();

        //Defining partial methods
        partial void PartialMethod()
        {
            Console.WriteLine("Partial Class's Partial method");

        }
    }

    public class employee : PartialClassOne
    {
        public void PartialMethod()
        {
            Console.WriteLine("Child Class's Partial method");
        }
    }

    public interface IEmployee
    {
        void EmployeeMethod();
    }

}
